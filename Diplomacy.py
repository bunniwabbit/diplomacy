# Diplomacy.py 
import sys

# INPUT: Take in one line of action 
# OUTPUT: Return a list with every word as its own element  
def read(s): 
    initial = s.strip() 
    return initial.split()


# INPUT: A list of every armies' actions 
# OUTPUT: A list of every army after the actions 
def diplomacy_eval(army_list):
    # make a dictionary to see which armies are supported 
    army_support_dict = {}

    # make a dictionary to see which army is supporting which
    # army_supporters[supporter] = supportee
    army_supporters = {}

    # populate the supporters 
    def support(army_list):
        for i in army_list: 
            if i[0] not in army_support_dict: 
                army_support_dict[i[0]] = 0
            if i[2] == "Support": 
                army_support_dict[i[3]] += 1
                army_supporters[i[0]] = i[3]

    # check where each army is supposed to end up 
    occupying_dict = {}
    def location(army_list):
        for i in army_list: 
            if i[2] == "Move":
                if i[3] not in occupying_dict: 
                    occupying_dict[i[3]] = []
                occupying_dict[i[3]].append(i[0])
            elif i[2] == "Hold" or i[2] == "Support": 
                if i[1] not in occupying_dict: 
                    occupying_dict[i[1]] = []
                occupying_dict[i[1]].append(i[0])

    # make a dictionary to see round results
    results = {}

    # if they are in the same city, check the number of their supports 
    def checkSupport(army_list):
        for i in occupying_dict: 
            # there are multiple armies occupyinng a city
            if len(occupying_dict[i]) > 1: # check to see if there is support 
                current = [] # make a list of armies in specific (current) city
                
                for j in occupying_dict[i]: 
                    current.append(army_support_dict[j])

                # find the winning army 
                max_support = max(current)
                memo = []

                # check if there is a single prevailing army or if they're all dead
                for j in occupying_dict[i]:
                    if army_support_dict[j] < max_support: 
                        results[j] = "[dead]"
                    else: 
                        memo.append(j)

                if len(memo) > 1: 
                    for k in memo: 
                        results[k] = "[dead]"
                else:
                    results[memo[0]] = i
            else:
                results[occupying_dict[i][0]] = i

    # apply the subeval functions to the army list 
    support(army_list)
    location(army_list)
    checkSupport(army_list)

    # to correct the number of supporters in case some of them die 
    rK = list(results.keys())
    rV = list(results.values())
    sK = list(army_supporters.keys())
    sV = list(army_supporters.values())

    for i in range(len(rV)):
        if (rV[i] != "[dead]"): # if there's someone who's alive
            if (army_support_dict[rK[i]] != 0): # check if they have support
                index = sV.index(rK[i])
                if (results[sK[index]] == "[dead]"): # check if the supporter is dead
                    army_support_dict[rK[i]] -= 1
                    results.clear()
                    checkSupport(army_list)
    
    # convert results dict into a 2D list
    results_list = list(results.items())
    results_list.sort()

    # see who wins 
    return results_list

# Print out the army's result 
def diplomacy_print(w, result): 
    w.write(f'{result[0]} {result[1]}\n')


# INPUT: a text file containing all of the armies and their locations/movements 
# OUTPUT: results of the armies' movements 
def diplomacy_solve(r, w):
    # check if the file is empty 
    assert(len(str(r)) != 0)

    army_list = []

    for s in r: 
        army_list.append(read(s))

    results = diplomacy_eval(army_list)

    # check that the output is in alphabetical order 
    check_results = sorted(results)
    assert(results == check_results)

    # print each line in results 
    for line in results: 
        diplomacy_print(w, line)

#!/usr/bin/env python3

# ------------------------------
# projects/diplomacy/RunDiplomacy.py
# ------------------------------

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)


## CHANGE THE BOTTOM     

""" #pragma: no cover
$ cat RunDiplomacy1.in




$ python RunCollatz.py < RunCollatz.in > RunCollatz.out

$ cat RunCollatz.out
1 10 20
100 200 125
201 210 89
900 1000 174



$ python -m pydoc -w Collatz
# That creates the file Collatz.html
"""